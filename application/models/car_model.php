<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Car_model extends MY_Model
{
	public function __construct() {
		parent::__construct();
	}

	public $_table = 'car';
	public $primary_key = 'Cid';

	public function checkPassword($email, $password) {
		$this->db->where('Uemail', $email);
		$this->db->where('Upassword', $password);
		$query = $this->db->get('user');

		return $query->num_rows() == 1 ? true : false;
	}
}
