<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| URL to your CodeIgniter root. Typically this will be your base URL,
| WITH a trailing slash:
|
|	http://example.com/
|
| If this is not set then CodeIgniter will guess the protocol, domain and
| path to your installation.
|
*/
$config['base_url']	= 'http://localhost/codeigniter2-skeleton';

$config['site_title'] = 'Title';
$config['site_description'] = 'Description';
$config['site_author'] = 'Author';
$config['site_keywords'] = 'Keywords';

/* End of file custom.php */
/* Location: ./application/config/development/custom.php */
